import React from 'react';
import { Link } from 'react-router-dom';
import ChatSideImage from '../img/banner-logo-2.jpg';

const Chatlist = () => {
  const handleRefresh = () => {
    console.log('Button Clicked');
  };
  return (
    <>
      <h1 className="text-3xl  mt-10 text-center">Welcome User</h1>
      <div className="container flex">
        <div className="user__list w-1/2 ml-10 pl-20 py-20 text-xl leading-loose">
          <div className="refresh flex gap-20 items-baseline justify-start">
            <h1 className="text-center text-4xl mb-10">Friends</h1>

            <Link
              onClick={handleRefresh}
              to="/chatList"
              className=" bg-sky-500 px-12
				py-1				
		        rounded-xl
	            text-center"
            >
              Refresh
            </Link>
          </div>

          <div
            className="card 
				max-w-sm
				overflow-hidden
				shadow-lg
				shadow-slate-500
				mb-7
				flex
				items-center"
          >
            <div className="card__inbox px-6 py-4 flex w-full">
              <p className="w-1/2">Rojan Adhikari</p>
              <Link
                to="/chat"
                className=" bg-sky-500 px-1
                    w-1/2
		            py-1
		            rounded-xl
	            	text-center"
              >
                Chat
              </Link>
            </div>
          </div>

          {/* <!-- User List Item -2 --> */}

          <div
            className="card 
				max-w-sm
				overflow-hidden
				shadow-lg
				shadow-slate-500
				mb-7
				flex
				items-center"
          >
            <div className="card__inbox px-6 py-4 flex w-full">
              <p className="w-1/2">Krishna Rijal</p>
              <Link
                to="/chat"
                className=" bg-sky-500 px-1
                    w-1/2
		            py-1
		            rounded-xl		          
	            	text-center"
              >
                Chat
              </Link>
            </div>
          </div>

          {/* <!-- User Item List 3 --> */}

          <div
            className="card 
				max-w-sm
				overflow-hidden
				shadow-lg
				shadow-slate-500
				mb-7
				flex
				items-center"
          >
            <div className="card__inbox px-6 py-4 flex w-full">
              <p className="w-1/2">Ujjwal Pudasaini</p>
              <Link
                to="/chat"
                className=" bg-sky-500 px-1
                    w-1/2
		            py-1
		            rounded-xl
	            	text-center"
              >
                Chat
              </Link>
            </div>
          </div>

          {/* <!-- User Item List 4 --> */}

          <div
            className="card 
				max-w-sm
				overflow-hidden
				shadow-lg
				shadow-slate-500
				mb-7
				flex
				items-center"
          >
            <div className="card__inbox px-6 py-4 flex w-full">
              <p className="w-1/2">Sabin Gautam</p>
              <Link
                to="/chat"
                className=" bg-sky-500 px-1
                w-1/2
                py-1
                rounded-xl
                text-center"
              >
                Chat
              </Link>
            </div>
          </div>
        </div>

        <div className="w-1/2 pt-20 pl-20 text-center m-auto">
          <img src={ChatSideImage} alt="Chat side bar image" />
        </div>
      </div>
    </>
  );
};

export default Chatlist;
