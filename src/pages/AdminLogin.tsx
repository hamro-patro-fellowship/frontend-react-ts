import React from 'react';
import { Link } from 'react-router-dom';
import BannerImage from '../img/banner-img.jpg';
import ChatLogo from '../img/chatlogo.png';
const AdminLogin = () => {
  return (
    <>
      <div className="section flex gap-8 w-full">
        <div className="section__left flex flex-col justify-start p-8">
          <div className="ml-80 p-10 mb-16 text-xl flex">
            <div className="logo">
              <img src={ChatLogo} alt="Chat Logo" />
            </div>

            <nav>
              <ul className="flex gap-12 px-4 ">
                <li>
                  <Link to="/userLogin">User</Link>
                </li>
                <li>
                  <Link to="/adminLogin">Admin</Link>
                </li>
              </ul>
            </nav>
          </div>

          <div className="form__layout">
            <form className="px-4 py-3 rounded-full flex justify-center flex-col w-auto">
              <input
                className="p-10 bg-gray-300
				placeholder-opacity-70
				placeholder-gray-900"
                type="text"
                placeholder="Admin ID"
              />
              <br />
              <input
                className="p-10 bg-gray-300
				placeholder-gray-900
				 placeholder-opacity-70"
                type="text"
                placeholder="Admin Name"
              />
              <br />

              <button
                type="submit"
                className="bg-sky-500 px-10 py-3  rounded w-40 text-xl"
              >
                Login
              </button>
            </form>
          </div>
        </div>

        <div className="section__right bg-gradient-to-t from-sky-50 to-blue-500">
          <div className="banner-image">
            <img src={BannerImage} alt="Banner Image" />
          </div>
        </div>
      </div>
    </>
  );
};

export default AdminLogin;
