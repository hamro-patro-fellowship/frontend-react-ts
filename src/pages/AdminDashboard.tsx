import React from 'react';
import { Link } from 'react-router-dom';

const AdminDashboard = () => {
  return (
    <>
      <h1 className="text-3xl  mt-10 text-center">Admin Dashboard</h1>
      <div className="container flex">
        <div className="user__list w-1/2 ml-10 pl-20 py-20 text-xl leading-loose">
          <h1 className="text-left text-4xl mb-10">User List</h1>

          <div
            className="card 
		max-w-3xl
		overflow-hidden
		shadow-lg
		shadow-slate-500
		mb-4
		flex
		items-center"
          >
            <div className="card__inbox px-6 py-4 flex gap-5 items-center ">
              <p className="text-center w-1/2">Rojan Adhikari</p>

              <Link
                to="/updateUser"
                className=" bg-sky-500 px-1
					py-1
					rounded-xl
					w-40
					text-center
					"
              >
                Update
              </Link>
              <Link
                to="/deleteUser"
                className=" bg-sky-500 px-1 ml-5
				py-1
				rounded-xl
				w-40
				text-center"
              >
                Delete
              </Link>
            </div>
          </div>

          {/* <!-- User List Item -2 --> */}

          <div
            className="card 
	max-w-xl
	overflow-hidden
	shadow-lg
	shadow-slate-500
	mb-7
	flex
	items-center"
          >
            <div className="card__inbox px-6 py-4 flex gap-5 items-center">
              <p className="text-center w-1/2">Krishna Rijal</p>

              <Link
                to="/updateUser"
                className=" bg-sky-500 px-1
			py-1
			rounded-xl
			w-40
			text-center"
              >
                Update
              </Link>

              <Link
                to="/deleteUser"
                className=" bg-sky-500 px-1 ml-5
		py-1
		rounded-xl
		w-40
		text-center"
              >
                Delete
              </Link>
            </div>
          </div>

          {/* <!-- User Item List 3 --> */}

          <div
            className="card 
max-w-xl
overflow-hidden
shadow-lg
shadow-slate-500
mb-7
flex
items-center"
          >
            <div className="card__inbox px-6 py-4 flex gap-5 items-center">
              <p className="text-center w-1/2">Ujjwal Pudasaini</p>

              <Link
                to="/updateUser"
                className=" bg-sky-500 px-1
		py-1
		rounded-xl
		w-40
		text-center"
              >
                Update
              </Link>

              <Link
                to="/deleteUser"
                className=" bg-sky-500 px-1 ml-5
	py-1
	rounded-xl
	w-40
	text-center"
              >
                Delete
              </Link>
            </div>
          </div>

          {/* <!-- User Item List 4 --> */}

          <div
            className="card 
	max-w-xl
	overflow-hidden
	shadow-lg
	shadow-slate-500
	mb-7
	flex
	items-center"
          >
            <div className="card__inbox px-6 py-4 flex gap-5 items-center">
              <p className="text-center w-1/2">Sabin Gautam</p>

              <Link
                to="/updateUser"
                className=" bg-sky-500 px-1
			py-1
			rounded-xl
			w-40
			text-center"
              >
                Update
              </Link>

              <a
                href="/deleteUser"
                className=" bg-sky-500 px-1 ml-5
		py-1
		rounded-xl
		w-40
		text-center"
              >
                Delete
              </a>
            </div>
          </div>
        </div>

        <div className="w-1/2 pt-20 pl-20 text-center m-auto">
          <form className="px-4 py-3 rounded-full flex justify-center flex-col ">
            <Link
              to="/addUser"
              type="submit"
              className="bg-sky-500 px-10 py-5 rounded-xl w-60 text-center text-2xl"
            >
              Add User
            </Link>
          </form>
        </div>
      </div>
    </>
  );
};

export default AdminDashboard;
