import React from 'react';
import { Link } from 'react-router-dom';

import BannerImage from '../img/banner-img.jpg';
const AddUser = () => {
  return (
    <>
      <div className="section flex gap-8 w-full">
        <div className="section__left flex flex-col justify-start p-8 w-1/2">
          <div className="ml-40 px-10 mb-20 text-xl flex">
            <div className="logo flex gap-20">
              <h1 className="text-left text-3xl">Add User</h1>
            </div>
          </div>

          <div className="form__layout">
            <form className="px-4 py-3 rounded-full flex justify-center flex-col w-auto">
              {/* <input
                className="p-10 bg-gray-300
				placeholder-opacity-70
				placeholder-gray-900"
                type="text"
                placeholder="User ID"
              />
              <br /> */}
              <input
                className="p-10 bg-gray-300
				placeholder-gray-900
				 placeholder-opacity-70"
                type="text"
                placeholder="User Name"
              />
              <br />

              <Link
                to={'#'}
                type="submit"
                className="bg-sky-500 px-10 py-5  rounded w-52 text-xl text-center"
              >
                Add User
              </Link>
            </form>
          </div>
        </div>

        <div className="section__right bg-gradient-to-t from-sky-50 to-blue-400">
          <img src={BannerImage} alt="Banner Image" className="p-20" />
        </div>
      </div>
    </>
  );
};

export default AddUser;
