import React from 'react';
import { Link } from 'react-router-dom';
import BannerImage from '../img/banner-img.jpg';
import ChatLogo from '../img/chatlogo.png';
import { ChatServiceClient } from '../GeneratedTS/Proto/ChatProtoServiceClientPb';
import { User } from '../GeneratedTS/Proto/ChatProto_pb';
const UserLogin = () => {
  const client = new ChatServiceClient(`http://localhost:8080`, null, null);
  let user = new User();
  // user.setId(Date.now());

  const refreshHandler = (e: any) => {
    e.preventDefault();
    client.join(user, null, (err, res) => {
      if (err) {
        return console.error(err);
      } else {
        window.alert('Refresh Handler Clicked');
      }
    });
  };
  return (
    <>
      <div className="section flex gap-8 w-full">
        <div className="section__left flex flex-col justify-start p-8 w-1/2">
          <div className="ml-80 p-10 mb-16 text-xl flex">
            <div className="logo">
              <img src={ChatLogo} alt="Chat Logo" />
            </div>

            <nav>
              <ul className="flex gap-12 px-4 ">
                <li>
                  <Link to="/userLogin">User</Link>
                </li>
                <li>
                  <Link to="/adminLogin">Admin</Link>
                </li>
              </ul>
            </nav>
          </div>

          <div className="form__layout">
            <form
              className="px-4 py-3 rounded-full flex justify-center flex-col w-auto"
              onSubmit={refreshHandler}
            >
              <input
                className="p-10 bg-gray-300
				placeholder-opacity-70
				placeholder-gray-900"
                type="text"
                placeholder="User ID"
              />
              <br />
              <input
                className="p-10 bg-gray-300
				placeholder-gray-900
				 placeholder-opacity-70"
                type="text"
                placeholder="User Name"
              />
              <br />

              <button
                type="submit"
                className="bg-sky-500 px-10 py-3  rounded w-40 text-xl"
              >
                Login
              </button>
            </form>
          </div>
        </div>

        <div className="section__right  w-1/2">
          <div className="banner-image bg-gradient-to-t from-sky-50 to-blue-400 p-20">
            <img src={BannerImage} alt="Banner Image" />
          </div>
        </div>
      </div>
    </>
  );
};

export default UserLogin;
