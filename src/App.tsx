import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import AddUser from './pages/AddUser';
import AdminDashboard from './pages/AdminDashboard';
import AdminLogin from './pages/AdminLogin';
import Chat from './pages/Chat';
import Chatlist from './pages/Chatlist';
import UpdateUser from './pages/UpdateUser';
import UserLogin from './pages/UserLogin';
const App = () => {
  return (
    <Router>
      <Routes>
        <Route path="/" element={<UserLogin />} />

        <Route path="/adminLogin" element={<AdminLogin />} />

        <Route path="/chat" element={<Chat />} />

        <Route path="/chatList" element={<Chatlist />} />

        <Route path="/updateUser" element={<UpdateUser />} />
        <Route path="/addUser" element={<AddUser />} />

        <Route path="/admin" element={<AdminDashboard />} />
      </Routes>
    </Router>
  );
};

export default App;
