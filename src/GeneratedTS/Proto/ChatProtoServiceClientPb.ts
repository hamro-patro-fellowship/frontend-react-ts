/**
 * @fileoverview gRPC-Web generated client stub for 
 * @enhanceable
 * @public
 */

// GENERATED CODE -- DO NOT EDIT!


/* eslint-disable */
// @ts-nocheck


import * as grpcWeb from 'grpc-web';

import * as Proto_ChatProto_pb from '../Proto/ChatProto_pb';


export class ChatServiceClient {
  client_: grpcWeb.AbstractClientBase;
  hostname_: string;
  credentials_: null | { [index: string]: string; };
  options_: null | { [index: string]: any; };

  constructor (hostname: string,
               credentials?: null | { [index: string]: string; },
               options?: null | { [index: string]: any; }) {
    if (!options) options = {};
    if (!credentials) credentials = {};
    options['format'] = 'text';

    this.client_ = new grpcWeb.GrpcWebClientBase(options);
    this.hostname_ = hostname;
    this.credentials_ = credentials;
    this.options_ = options;
  }

  methodInfojoin = new grpcWeb.MethodDescriptor(
    '/ChatService/join',
    grpcWeb.MethodType.UNARY,
    Proto_ChatProto_pb.User,
    Proto_ChatProto_pb.JoinResponse,
    (request: Proto_ChatProto_pb.User) => {
      return request.serializeBinary();
    },
    Proto_ChatProto_pb.JoinResponse.deserializeBinary
  );

  join(
    request: Proto_ChatProto_pb.User,
    metadata: grpcWeb.Metadata | null): Promise<Proto_ChatProto_pb.JoinResponse>;

  join(
    request: Proto_ChatProto_pb.User,
    metadata: grpcWeb.Metadata | null,
    callback: (err: grpcWeb.RpcError,
               response: Proto_ChatProto_pb.JoinResponse) => void): grpcWeb.ClientReadableStream<Proto_ChatProto_pb.JoinResponse>;

  join(
    request: Proto_ChatProto_pb.User,
    metadata: grpcWeb.Metadata | null,
    callback?: (err: grpcWeb.RpcError,
               response: Proto_ChatProto_pb.JoinResponse) => void) {
    if (callback !== undefined) {
      return this.client_.rpcCall(
        this.hostname_ +
          '/ChatService/join',
        request,
        metadata || {},
        this.methodInfojoin,
        callback);
    }
    return this.client_.unaryCall(
    this.hostname_ +
      '/ChatService/join',
    request,
    metadata || {},
    this.methodInfojoin);
  }

  methodInfosendMsg = new grpcWeb.MethodDescriptor(
    '/ChatService/sendMsg',
    grpcWeb.MethodType.UNARY,
    Proto_ChatProto_pb.ChatMessage,
    Proto_ChatProto_pb.Empty,
    (request: Proto_ChatProto_pb.ChatMessage) => {
      return request.serializeBinary();
    },
    Proto_ChatProto_pb.Empty.deserializeBinary
  );

  sendMsg(
    request: Proto_ChatProto_pb.ChatMessage,
    metadata: grpcWeb.Metadata | null): Promise<Proto_ChatProto_pb.Empty>;

  sendMsg(
    request: Proto_ChatProto_pb.ChatMessage,
    metadata: grpcWeb.Metadata | null,
    callback: (err: grpcWeb.RpcError,
               response: Proto_ChatProto_pb.Empty) => void): grpcWeb.ClientReadableStream<Proto_ChatProto_pb.Empty>;

  sendMsg(
    request: Proto_ChatProto_pb.ChatMessage,
    metadata: grpcWeb.Metadata | null,
    callback?: (err: grpcWeb.RpcError,
               response: Proto_ChatProto_pb.Empty) => void) {
    if (callback !== undefined) {
      return this.client_.rpcCall(
        this.hostname_ +
          '/ChatService/sendMsg',
        request,
        metadata || {},
        this.methodInfosendMsg,
        callback);
    }
    return this.client_.unaryCall(
    this.hostname_ +
      '/ChatService/sendMsg',
    request,
    metadata || {},
    this.methodInfosendMsg);
  }

  methodInforeceiveMsg = new grpcWeb.MethodDescriptor(
    '/ChatService/receiveMsg',
    grpcWeb.MethodType.UNARY,
    Proto_ChatProto_pb.Empty,
    Proto_ChatProto_pb.ChatMessage,
    (request: Proto_ChatProto_pb.Empty) => {
      return request.serializeBinary();
    },
    Proto_ChatProto_pb.ChatMessage.deserializeBinary
  );

  receiveMsg(
    request: Proto_ChatProto_pb.Empty,
    metadata: grpcWeb.Metadata | null): Promise<Proto_ChatProto_pb.ChatMessage>;

  receiveMsg(
    request: Proto_ChatProto_pb.Empty,
    metadata: grpcWeb.Metadata | null,
    callback: (err: grpcWeb.RpcError,
               response: Proto_ChatProto_pb.ChatMessage) => void): grpcWeb.ClientReadableStream<Proto_ChatProto_pb.ChatMessage>;

  receiveMsg(
    request: Proto_ChatProto_pb.Empty,
    metadata: grpcWeb.Metadata | null,
    callback?: (err: grpcWeb.RpcError,
               response: Proto_ChatProto_pb.ChatMessage) => void) {
    if (callback !== undefined) {
      return this.client_.rpcCall(
        this.hostname_ +
          '/ChatService/receiveMsg',
        request,
        metadata || {},
        this.methodInforeceiveMsg,
        callback);
    }
    return this.client_.unaryCall(
    this.hostname_ +
      '/ChatService/receiveMsg',
    request,
    metadata || {},
    this.methodInforeceiveMsg);
  }

  methodInfogetAllUsers = new grpcWeb.MethodDescriptor(
    '/ChatService/getAllUsers',
    grpcWeb.MethodType.UNARY,
    Proto_ChatProto_pb.Empty,
    Proto_ChatProto_pb.UserList,
    (request: Proto_ChatProto_pb.Empty) => {
      return request.serializeBinary();
    },
    Proto_ChatProto_pb.UserList.deserializeBinary
  );

  getAllUsers(
    request: Proto_ChatProto_pb.Empty,
    metadata: grpcWeb.Metadata | null): Promise<Proto_ChatProto_pb.UserList>;

  getAllUsers(
    request: Proto_ChatProto_pb.Empty,
    metadata: grpcWeb.Metadata | null,
    callback: (err: grpcWeb.RpcError,
               response: Proto_ChatProto_pb.UserList) => void): grpcWeb.ClientReadableStream<Proto_ChatProto_pb.UserList>;

  getAllUsers(
    request: Proto_ChatProto_pb.Empty,
    metadata: grpcWeb.Metadata | null,
    callback?: (err: grpcWeb.RpcError,
               response: Proto_ChatProto_pb.UserList) => void) {
    if (callback !== undefined) {
      return this.client_.rpcCall(
        this.hostname_ +
          '/ChatService/getAllUsers',
        request,
        metadata || {},
        this.methodInfogetAllUsers,
        callback);
    }
    return this.client_.unaryCall(
    this.hostname_ +
      '/ChatService/getAllUsers',
    request,
    metadata || {},
    this.methodInfogetAllUsers);
  }

}

